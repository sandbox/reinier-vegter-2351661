/**
 * Created by reinier on 5-10-14.
 */

jQuery(function ($) {
    $.stellar({
        verticalScrolling: true,
        horizontalScrolling: false,
    });

    resetParallax = function(id, flat, stretch) {
        if (!flat) {
            selector = "#" + id + " .parallax-container";
        }
        else {
            selector = id;
        }

        if (stretch == 1) {
            jQuery(selector).css("width", jQuery(window).width());
            jQuery(selector).css("position", "absolute");
            jQuery(selector).offset({left:0});
        }
    };

    initParallax = function (id, flat, height, imageHeight, url, dropshadow, offset, scrollratio) {

        if (!flat) {
            jQuery("#" + id + " .embed-container").css("position", "relative");
            jQuery("#" + id + " .parallax-container img").css("visibility", "hidden");
            selector = "#" + id + " .parallax-container";
        }
        else {
            selector = id;
        }

        jQuery(selector).css("background-image", "url(\"" + url + "\")");

        if (height) {
            jQuery(selector).css("height", height);
        }

        if (imageHeight > 0) {
            jQuery(selector).css("background-size", imageHeight + "px auto");
        }
        else {
            jQuery(selector).css("background-size", "100%");
        }

        if (offset) {
            jQuery(selector).attr("data-stellar-vertical-offset", offset);
        }
        else {
            element = jQuery(selector);
            elementOffset = element.offset();
            elementTop = elementOffset.top;
            if (height) {
                elementTop = elementTop + (height / 2);
            }

            if (scrollratio) {
                elementTop = elementTop / scrollratio;
            }

            jQuery(selector).attr("data-stellar-vertical-offset", elementTop);
            //jQuery(selector).css("background-position", "center");
        }

        if (dropshadow) {
            shadow = "inset  0  8px 25px -8px #000, inset  0 -8px 25px -8px #000";
            jQuery(selector).css("-webkit-box-shadow", shadow);
            jQuery(selector).css("-moz-box-shadow", shadow);
            jQuery(selector).css("box-shadow", shadow);
        }

    };
});
