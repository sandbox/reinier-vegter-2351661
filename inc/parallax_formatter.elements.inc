<?php
/**
 * Created by PhpStorm.
 * User: reinier
 * Date: 6-10-14
 * Time: 20:52
 */

function parallax_formatter_tweak_elements() {
  $elements = variable_get('parallax_formatter_elements', '');
  if (!empty($elements)) {

    // Split by newlines.
    $elements = preg_split('/\r\n|\r|\n/', $elements);
    foreach ($elements as $element) {
      $parts = explode('|', $element);
      $selector = $parts[0];
      if (isset($parts[3])) {
        $stretch = $parts[1];
        $drop_shadow = $parts[2];
        $image_uri = $parts[3];

        if (isset($parts[4])) {
          $offset = $parts[4];
        }
        else {
          $offset = 0;
        }

        $flat = 1;
        $height = 0;
        $image_height = 0;
        $scroll_ratio = 0.8;

        // Add javascript.
        drupal_add_js('jQuery(document).ready(function() {
          jQuery("' . $selector . '").attr("data-stellar-background-ratio", "' . $scroll_ratio . '");
          initParallax("' . $selector . '", 1, ' . $height . ', ' . $image_height . ', "' . $image_uri . '", ' . $drop_shadow . ', ' . $offset . ', ' . $scroll_ratio . ');
          resetParallax("' . $selector . '", ' . $flat . ', ' . $stretch . ', ' . $offset . ');
          jQuery(window).resize(function() {resetParallax("' . $selector . '", ' . $flat . ', ' . $stretch . ');});
          jQuery(window).data("plugin_stellar").destroy();
          jQuery(window).data("plugin_stellar").init();

        });', 'inline');
      }
    }
  }
}
