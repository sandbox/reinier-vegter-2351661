<?php
/**
 * Created by PhpStorm.
 * User: reinier
 * Date: 6-10-14
 * Time: 20:34
 */

function parallax_formatter_elements_admin($form, &$form_state) {
  $description = 'Define an element on each row.  <br />';
  $description .= 'Just put "[selector]|[stretch: 1/0]|[dropshadow: 1/0]|[file_uri]|[offset px]" <br />';
  $description .= 'Example: <br />';
  $description .= '#header|1|0|http://fancy_drupal.dev/sites/default/files/1900x1200.jpg|200 <br />';

  $form['parallax_formatter_elements'] = array(
    '#type' => 'textarea',
    '#description' => $description,
    '#title' => t('Elements vs backgrounds'),
    '#default_value' => variable_get('parallax_formatter_elements', ''),
  );

  $form = system_settings_form($form);
  return $form;
}
